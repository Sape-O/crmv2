import React, { Component } from 'react'
import axios from 'axios'
import {Button} from 'primereact/button'
import {DataTable} from 'primereact/datatable'
import {Column} from 'primereact/column'
import {InputText} from 'primereact/inputtext'
import {Dialog} from 'primereact/dialog'
import {InputTextarea} from 'primereact/inputtextarea'
import {Calendar} from 'primereact/calendar'
import {Dropdown} from 'primereact/dropdown'
import {Growl} from 'primereact/growl'
import {url_react,url_server} from '../../service/url_part'

export default class Contact extends Component {
  constructor() {
        super();
        this.state = {
          visible: false,
					contactData:'',
          Contact_log_Id:'',
          Customer_Id:'',
          Date_contact:'',
          Method:'',
          Notes:''
        }

  }

  getContact() {

    const id  = this.props.match.params.id
    this.setState({Customer_Id:id})
    let self = this

    axios.get(url_server + '/contact/log/' + id)
    .then(response => {
      if(response.data == "0")
        alert("Error query Contact (react, componentDidMount)")
      else {
        self.setState({ contactData: response.data})
      }
    })
    .catch(function (error) {
      alert('Error for query DB (catch react)\n' + error)
    })
  }

  componentDidMount() {
    this.getContact()
  }


  saveContact() {


    if(this.state.Date_contact == '')
    this.growl.show({severity: 'error', summary: 'Error Message', detail: 'Date Contact Incorrect !'})
    else if (this.state.Method == '' || this.state.Method.length > 100)
    this.growl.show({severity: 'error', summary: 'Error Message', detail: 'Method Incorrect !'})
    else if (this.state.Notes == '' || this.state.Notes >100)
    this.growl.show({severity: 'error', summary: 'Error Message', detail: 'Notes Incorrect !'})

    else {

      this.setState({visible: false})
      // vallidate of data befor send
      let self = this
      if(this._newContact === true) {
        axios.post(url_server + '/contact/newcontact/' + this.state.Customer_Id, {
          Date_contact: this.state.Date_contact,
          Method: this.state.Method,
          Notes: this.state.Notes
        })
        .then(function (response) {
          if (response.data == "Success"){
            self.getContact() // call function to getdata it will auto update table
            alert(response.data)
          } else
            alert(response.data)

        })
      } else if(this._newContact === false) {

        axios.post(url_server + '/contact/edit/' + this.state.Contact_log_Id, {
          Date_contact: this.state.Date_contact,
          Method: this.state.Method,
          Notes: this.state.Notes
        })
        .then(function (response) {
          if (response.data == "0") {
            alert("Error for Edit Success (react, saveContact)")
          } else {
            self.getContact() // call function to getdata it will auto update table
            alert(response.data)
            self.setState({
              Contact_log_Id:'',
              Date_contact:'',
              Method:'',
              Notes:''
            })
          }
        })
        .catch(function (error) {
          alert('Error for query DB (catch react)\n' + error)
        })
      }
    }

  }
  export() {

  }
  newContact() {
    this._newContact = true
    this.setState({visible: true})
  }
  onHide(event) {
    this.setState({visible: false})
    this.setState({Contact_log_Id:'', Date_contact:'', Method:'', Notes:''})
  }
  editButton(e) {
    this._newContact = false
    this.setState({visible: true})

    // change dd-mm-yyy to Date formate
    let pattern = /(\d{2})\-(\d{2})\-(\d{4})/
    let dt = new Date(e.Date_contact.replace(pattern,'$3-$2-$1'))

    this.setState({
      Contact_log_Id: e.Contact_log_Id,
      Date_contact: dt,
      Method: e.Method,
      Notes: e.Notes,


    })
  }

  actionTemplate(rowData) {

        return <div>
                  <span
                    type="button"
                    className="button is-warning is-focused"
                    style={{width: '40px', margin:'3px'}}
                    onClick={this.editButton.bind(this,rowData)}
                  >Edit</span>
                  <span
                    disabled
                    type="button"
                    className="button is-danger is-focused"
                    style={{width: '70px', margin:'3px'}}
                  >Delete</span>
              </div>
    }


  render() {
		const MethodSelectItems = [
			{label: 'Email', value: 'Email'},
			{label: 'Telephone', value: 'Telephone'},
			{label: 'Fax', value: 'Fax'},
			{label: 'Common', value: 'Common'}
		]
    var header =<div>
                  <div style={{'textAlign':'left'}}>
                    <i className="pi pi-search" style={{margin:'4px 4px 0 0'}} />
                    <InputText
                      type="search"
                      placeholder="Global Search"
                      size="50"
                      onInput={(e) => this.setState({globalFilter: e.target.value})}
                    />
                  </div>
                  <div style={{'textAlign':'right'}}>
                    <Button
                      disabled
                      type="button"
                      icon="pi pi-external-link"
                      iconPos="left"
                      label="Export CSV"
                      onClick={this.export.bind(this)}
                      style={{'textAlign':'right'}}
                    />
                  </div>
                </div>

    const footer = (
            <div>

              <a class="button is-info is-outlined" style={{marginRight:'1%'}}>
                <span class="icon is-small">
                  <i class="fas fa-check"></i>
                </span>
                <span style={{width:'100px'}} onClick={this.saveContact.bind(this)}>
                  {this.state.Contact_log_Id? "Edit":"Save"}
                </span>
              </a>

              <a class="button is-danger is-outlined" style={{marginLeft:'1%'}}>
                <span style={{width:'100px'}} onClick={this.onHide.bind(this)}>Cancel</span>
                <span class="icon is-small">
                  <i class="fas fa-times"></i>
                </span>
              </a>

            </div>
        )

    return(
            <div className="box" style={{marginTop:"10px",marginLeft:"6px",marginRight:"6px"}}>
              <p className="title is-1">Contact</p>
              <div>

                <span
                  className="button is-info is-outlined is-medium"
                  onClick={this.newContact.bind(this)}
                  style={{marginBottom:'20px',marginTop:'20px'}}
                >New Contact</span>

                <Dialog
									header={this.state.Contact_log_Id ? "Edit Contact":"New Contact"}
									footer={footer}
									visible={this.state.visible}
									style={{width: '24vw'}}
									modal={true}
									onHide={this.onHide.bind(this)}
								>
                  <div>
                    <div className="tile is-ancestor">
                        <div className="tile">
                          <div className="tile is-parent is-vertical" >

                            <div className="field">
                              <label className="label">Date Contact </label>
                              <div className="control">
																<Calendar
																	dateFormat="dd-mm-yy"
																	value={this.state.Date_contact}
																	onChange={(e) => this.setState({Date_contact: e.target.value})}
																	showIcon={true}
																/>
                              </div>
                            </div>

                            <div className="field">
                              <label className="label">Method</label>
                              <div className="control">
																<Dropdown
																	placeholder="Select a Method"
																	value={this.state.Method}
																	options={MethodSelectItems}
																	onChange={(e) => {this.setState({Method: e.target.value})}}
																/>
                              </div>
                            </div>

	                          <div className="field">
	                            <label className="label">Owner</label>
	                            <div className="control">
	                              <input
																	disabled
	                                className="input is-primary"
	                                type="text"
	                                placeholder="Patiphan Winthachai"
	                                style={{width:'340px'}}
	                              />
	                            </div>
	                          </div>

                            <div className="field">
                              <label className="label">Note</label>
                              <div className="control">
																<InputTextarea
																	rows={5}
																	cols={30}
																	placeholder="Notes"
																	style={{width:'340px',height:'197px'}}
																	value={this.state.Notes}
																	onChange={(e) => this.setState({Notes:e.target.value})}
																	autoResize={true}
																/>
                              </div>
                            </div>

                          </div>
                        </div>
                    </div>
                  </div>
                </Dialog>
              </div>

              <div className="content-section implementation">
                <DataTable
                  value={this.state.contactData}
                  paginator={true}
                  rows={30} rowsPerPageOptions={[30,50]}
                  header={header}
                  globalFilter={this.state.globalFilter}
                  selectionMode="single"
                  emptyMessage="No records found">
                  <Column field="Date_contact" header="Date Contact" sortable={true} filter={true} />
                  <Column field="Method" header="Method"  filter={true} />
                  <Column field="Notes" header="Notes"  filter={true} />
                  <Column field="Owner" header="Owner"  filter={true} />
                  <Column body={this.actionTemplate.bind(this)} style={{textAlign:'center', width: '150px'}}/>
                </DataTable>
              </div>

              <Growl ref={(el) => this.growl = el} />

            </div>
          )

  }

}
