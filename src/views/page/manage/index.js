import React,{Component} from 'react'
import axios from 'axios'
import {Growl} from 'primereact/growl';
import Dashboard from './dashboard'
import Members from './members'
import AddMember from './add_edit_member'
import PolicyTeamSetting from './policy_team_settings'
import Vendor from './vendor'
import AddVendor from './add_edit_vendor'
import TypeKind from './type_kind'
import {url_react,url_server} from '../../service/url_part'

export default class Management extends Component {
	constructor() {
		super()
		this.state={
			dataShow:[]

		}

	}

	//\/\/\/\/\/\\\/\/\/\/\/\----------- Click Manu ----------------//
	dashboardClick() {
		this.setState({dataShow: <Dashboard />})
	}

	//---------------- Members--------------//
	member = () => {
		this.setState({dataShow: <Members edit={this.editMember_props} />})
	}
	addMember = () => {
		this.setState({dataShow: <AddMember newUser={true} userEdit={0} />})
	}
	//------------ end Members--------------//

	//------------ vendor-------------------//

	vendor = () => {
		this.setState({dataShow: <Vendor edit={this.editVendor_props} />})
	}
	addVendor = () => {
		this.setState({dataShow: <AddVendor newVendor={true} vendorEdit={0} />})
	}
	//------------ vendor-------------------//
	policyTeamSetting() {
		this.setState({dataShow: <PolicyTeamSetting />})
	}
	typeKind() {
		this.setState({dataShow: <TypeKind />})
	}
	//\/\/\/\/\/\\\/\/\/\/\/\------- end Click Manu ------------------//




	//-------------- function props state------------//
	editMember_props = (e) => {
		this.setState({dataShow: <AddMember newUser={false} userEdit={e} linkmember={this.member} />})
	}
	editVendor_props = (e) => {
		this.setState({dataShow: <AddVendor newVendor={false} vendorEdit={e} linkvendor={this.vendor} />})
	}
	//---------- end function props state------------//




	render() {

		return(
			<div>
				<div class="columns" >
					<div class="column is-one-quarter" style={{width:'350px'}}>
						<div class="box" style={{marginTop:'10%', marginLeft:'10%'}}>
							<aside class="menu">
							  <p class="menu-label">
							    General
							  </p>
							  <ul class="menu-list">
							    <li><a onClick={this.dashboardClick.bind(this)}>Dashboard</a></li>
							  </ul>
							  <p class="menu-label">
							    Members Settings
							  </p>
							  <ul class="menu-list">
							    <li>
							      <ul>
							        <li><a onClick={this.member.bind(this)}>Members</a></li>
							        <li><a onClick={this.addMember.bind(this)}>Add a member</a></li>
							      </ul>
							    </li>
							  </ul>
								<p class="menu-label">
							    Vendor Settings
							  </p>
							  <ul class="menu-list">
									<li>
										<ul>
											<li><a onClick={this.vendor.bind(this)}>Vendor</a></li>
											<li><a onClick={this.addVendor.bind(this)}>Add Vendor</a></li>
										</ul>
									</li>
							  </ul>
								<p class="menu-label">
							    Other Settings
							  </p>
							  <ul class="menu-list">
									<li>
										<ul>
											<li><a onClick={this.typeKind.bind(this)}>Type Kind</a></li>
										</ul>
									</li>
							  </ul>
							</aside>
						</div>
				  </div>

				  <div class="column">
							{this.state.dataShow}
					 </div>
				</div>
				<Growl ref={(el) => this.growl = el} />
			</div>


		)
	}
}
