import React,{Component} from 'react'
import axios from 'axios'
import {DataTable} from 'primereact/datatable'
import {Column} from 'primereact/column'
import {Button} from 'primereact/button'
import {InputText} from 'primereact/inputtext'

import {url_react,url_server} from '../../service/url_part'

export default class Members extends Component {
	constructor() {
		super();
		this.state={
			vendor:''
		}

	}

	getVendor() {
		axios.get(url_server + '/vendor')
		.then(response => {
			if(response.data == "0")
				alert("Error query Contact (react, componentDidMount)")
			else
				this.setState({vendor: response.data })
		})
		.catch(function (error) {
			alert('Error for query DB (catch react)\n' + error)
		})
	}

	componentDidMount() {

		this.getVendor()
	}
	export() {

	}
	edit () {

	}
	actionTemplate(rowData) {
    //rowData, column
        return <div>
                  <button

                    type="button"
                    className="button is-warning is-focused"
                    style={{width: '40px', margin:'2px'}}
										onClick={this.props.edit.bind(this, rowData['Vendor_Id'])}
                  >Edit</button>
                  <button
										disabled
                    type="button"
                    className="button is-danger is-focused"
                    style={{width: '60px', margin:'2px'}}
                  >Delete</button>
              </div>
    }

	render() {
		var header =<div>
									<div style={{'textAlign':'left'}}>
										<i className="pi pi-search" style={{margin:'4px 4px 0 0'}} />
										<InputText type="search" onInput={(e) => this.setState({globalFilter: e.target.value})} placeholder="Global Search" size="50"/>
									</div>
									<div style={{'textAlign':'right'}}>
										<Button type="button" icon="pi pi-external-link" iconPos="left" label="Export CSV" onClick={this.export.bind(this)} style={{'textAlign':'right'}} disabled></Button>
									</div>
								</div>

		return(
			<div  style={{marginTop:'35px', marginRight:'1%'}}>
				<div className="content-section implementation">
					<DataTable
						value={this.state.vendor}
						paginator={true}
						rows={10} rowsPerPageOptions={[10,30,50]}
						header={header}
						selectionMode="single"
						emptyMessage="No records found">
						<Column field="NameProduct" header="Name Product" sortable={true} filter={true} />
						<Column field="Name_vendor" header="Name vendor"  filter={true} />
						<Column field="Email_vendor" header="Email"  filter={true} />
						<Column field="Tel_vendor" header="Telephone"  filter={true} />
						<Column body={this.actionTemplate.bind(this)} style={{textAlign:'center', width: '11%'}}/>
					</DataTable>
				</div>
			</div>

		)
	}
}
