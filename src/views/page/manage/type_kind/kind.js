import React,{Component} from 'react'
import axios from 'axios'
import {Dropdown} from 'primereact/dropdown'
import {Growl} from 'primereact/growl'
import {DataTable} from 'primereact/datatable'
import {Column} from 'primereact/column'
import {InputText} from 'primereact/inputtext'

import {url_react,url_server} from '../../../service/url_part'

export default class Kind extends Component {
	constructor() {
		super();
		this.state={
			typeSelect:[{label:'', value:''}],
			typeIdSelect:'',
			Name_kind_type_tech:'',
			kindData:''

		}
	}

	componentDidMount() {
		this.getTypeSelect()
		this.getKindData()
	}

	// get type for push to Dropdown
	getTypeSelect() {
		let self = this
		axios.get(url_server + '/type_technology')
    .then(response => {
      if(response.data == "0")
        alert("Error query Type Select (react, getTypeSelect(if))")
      else{
				let data = [...this.state.typeSelect]
				for (var i = 0; i < response.data.length; i++) {
					data[i].value = response.data[i].Type_technology_Id
					data[i].label = response.data[i].name_type_technology
					if(response.data.length-1 != i) {
						data=[...data, {label:'', value:''}]
					}
				}
				self.setState({typeSelect: data})
			}
    })
    .catch(function (error) {
      alert('Error for query DB (catch react getTypeSelect)\n' + error)
    })
	}

	getKindData() {
		let self = this
		axios.get(url_server + '/kind_type_tech')
    .then(response => {
      if(response.data == "0")
        alert("Error query Kind  (react, getKindData(if))")
      else{
				self.setState({kindData: response.data})
			}
    })
    .catch(function (error) {
      alert('Error for query DB (catch react getKindData)\n' + error)
    })
	}


	saveKind() {
		let self = this
		if(this.state.typeIdSelect == '' )
			this.growl.show({severity: 'error', summary: 'Save', detail: 'Please Select Type !'})
		else if (this.state.Name_kind_type_tech == '' || this.state.Name_kind_type_tech.length >= 100)
			this.growl.show({severity: 'error', summary: 'Save', detail: 'Insert Kind Incorrect !'})
		else {

			axios.post(url_server + '/management/newKind/', {
				typeIdSelect: this.state.typeIdSelect,
				Name_kind_type_tech: this.state.Name_kind_type_tech
			})
			.then(function (response) {

				if (response.data == "0")
					self.growl.show({severity: 'error', summary: 'Save', detail: 'Add Kind Fail !'})
				else if(response.data == "Success") {
					self.growl.show({severity: 'info', summary: 'Save', detail: 'Save '+response.data+' !'})
					self.setState({Name_kind_type_tech:''})
					self.getKindData()
				}else alert(response.data)

			})
			.catch(function (error) {
				alert('Error for query DB (catch react saveKind)\n' + error)
			})
		}

	}

	onEditorValueChange(props, value) {
    let updatedKindData = [...props.value]

		if(updatedKindData[props.rowIndex][props.field] != value)
    	updatedKindData[props.rowIndex][props.field] = value

		this.setState({kindData: updatedKindData})

  }

	kindEditor(props) {
		return <InputText type="text" value={props.rowData.Name_kind_type_tech} onChange={(e) => this.onEditorValueChange(props, e.target.value)} />
  }

	requiredValidator(props) {

		let value = props.rowData[props.field]
		let kindId = props.rowData['Kind_type_tech_Id']
		let self = this

		axios.post(url_server + '/management/editKind/' + kindId, {
			Name_kind_type_tech: props.rowData['Name_kind_type_tech']
		})
		.then(function (response) {

			if (response.data == "0")
				self.growl.show({severity: 'error', summary: 'Edit', detail: 'Edit Type Fail !'})

			else if(response.data == "Success") {
				self.growl.show({severity: 'info', summary: 'Edit', detail: 'Edit ' +value+ ' ' + response.data + ' !'})
				self.getKindData()

			}else alert(response.data)

		})
		.catch(function (error) {
			alert('Error for query DB (catch react requiredValidator)\n' + error)
		})

		return value && value.length > 0

	}

	actionTemplate(rowData) {
    //rowData, column
        return <div>
                  <button
										disabled
                    type="button"
                    className="button is-danger is-focused"
                    style={{width: '60px', margin:'2px'}}
                  >Delete</button>
              </div>
    }

	render() {
		return(

			<div>
				<h3 class="title is-3">{this.state.headName}</h3>
				<div style={{margin:'5px'}}>
					<div class="tile is-ancestor">
						<div class="tile is-vertical">
							<div class="tile">
								<div class="tile is-parent is-vertical">

									<article class="tile is-child  is-primary">
										<div class="tile is-ancestor">
											<div class="tile">
												<div className="field" style={{width:'90%', marginLeft:'13px'}}>
													<label className="label">Insert Kind</label>
													<div className="tile" >
															<Dropdown
																value={this.state.typeIdSelect}
																options={this.state.typeSelect}
																onChange={(e) => {this.setState({typeIdSelect: e.target.value})}}
																placeholder="Select Type"
																style={{width:'30%', marginRight:'1%'}}
															/>
															<input
																className="input is-primary"
																type="text"
																placeholder="Kind of Technology"
																style={{width:'50%'}}
																value={this.state.Name_kind_type_tech}
																onChange={(e) => {this.setState({Name_kind_type_tech: e.target.value})}}
															/>
															<span
																class="button is-info is-outlined"
																style={{marginLeft:'1%'}}
																onClick={this.saveKind.bind(this)}
															>Save Kind</span>
														</div>
													</div>
											</div>
										</div>
									</article>

									<DataTable
										value={this.state.kindData}
										paginator={true}
										rows={8}
										selectionMode="single"
										style={{width:'90%'}}
										editable={true}
										emptyMessage="No records found">
											<Column
											field="name_type_technology"
											header="Name Type"
											/>
											<Column
											field="Name_kind_type_tech"
											header="Name Kind"
											editor={this.kindEditor.bind(this)}
											editorValidator={this.requiredValidator.bind(this)}
											/>
											<Column body={this.actionTemplate.bind(this)} style={{textAlign:'center', width: '90px'}}/>
									</DataTable>

								</div>
							</div>
						</div>
					</div>
				</div>



				<Growl ref={(el) => this.growl = el} />

			</div>
		)
	}
}
