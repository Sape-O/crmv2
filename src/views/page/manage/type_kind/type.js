import React,{Component} from 'react'
import axios from 'axios'
import {Growl} from 'primereact/growl'
import {Dropdown} from 'primereact/dropdown'
import {DataTable} from 'primereact/datatable'
import {Column} from 'primereact/column'
import {InputText} from 'primereact/inputtext'
import {url_react,url_server} from '../../../service/url_part'

export default class Type extends Component {
	constructor() {
		super();
		this.state={
			name_type_technology:'',
			typeData:'',
			typeDataTemp:''

		}

	}

	componentDidMount() {

		this.getType()

	}

	// get Type for list show all
	getType() {
		let self = this
		axios.get(url_server + '/type_technology')
    .then(response => {
      if(response.data == "0")
        alert("Error query Customer (react, getTypeSelect)")
      else{
				self.setState({typeData: response.data, typeDataTemp: response.data})
			}
    })
    .catch(function (error) {
      alert('Error for query DB (catch react getPermission)\n' + error)
    })
	}

	// input data and click Save Type
	saveType() {

		let self = this
		if(this.state.name_type_technology == '' || this.state.name_type_technology >=100 )
			this.growl.show({severity: 'error', summary: 'Save', detail: 'type Incorrect !'})
		else {
			axios.post(url_server + '/management/newType/', {
				name_type_technology: this.state.name_type_technology
			})
			.then(function (response) {

				if (response.data == "0")
					self.growl.show({severity: 'error', summary: 'Save', detail: 'Add Type Fail !'})
				else if(response.data == "Success") {
					self.growl.show({severity: 'info', summary: 'Save', detail: 'Save '+response.data+' !'})
					self.setState({name_type_technology: ''}) // for bug bueatifull UI
					self.getType()
				}else alert(response.data)

			})
			.catch(function (error) {
				alert('Error for query DB (catch react saveType)\n' + error)
			})
		}

	}

	onEditorValueChange(props, value) {
    let updatedTypeData = [...props.value]
  	updatedTypeData[props.rowIndex][props.field] = value

		this.setState({typeData: updatedTypeData})

  }

	typeEditor(props) {
		return <InputText type="text" value={props.rowData.name_type_technology} onChange={(e) => this.onEditorValueChange(props, e.target.value)} />
  }

	requiredValidator(props) {

		let value = props.rowData[props.field]
		let typeId = props.rowData['Type_technology_Id']
		let self = this

		if(props.rowData['name_type_technology'] == '') {
			self.growl.show({severity: 'error', summary: 'Edit', detail: 'Type is Empty !'})
		}


		else {
			axios.post(url_server + '/management/editType/' + typeId, {
				name_type_technology: props.rowData['name_type_technology']
			})
			.then(function (response) {

				if (response.data == "0")
					self.growl.show({severity: 'error', summary: 'Edit', detail: 'Edit Type Fail !'})

				else if(response.data == "Success") {
					self.growl.show({severity: 'info', summary: 'Edit', detail: 'Edit ' +value+ ' ' + response.data + ' !'})

				}else alert(response.data)

			})
			.catch(function (error) {
				alert('Error for query DB (catch react requiredValidator)\n' + error)
			})
		}


		return value && value.length > 0

	}

	actionTemplate(rowData) {
    //rowData, column
        return <div>
                  <button
										disabled
                    type="button"
                    className="button is-danger is-focused"
                    style={{width: '60px', margin:'2px'}}
                  >Delete</button>
              </div>
    }

	render() {

		return(

			<div>
				<h3 className="title is-3">{this.state.headName}</h3>
				<div style={{margin:'5px'}}>
					<div className="tile is-ancestor">
						<div className="tile is-vertical">
							<div className="tile">
								<div className="tile is-parent is-vertical">

									<article className="tile is-child  is-primary">
										<div className="tile is-ancestor">
											<div className="tile">
												<div className="field" style={{width:'85%', marginLeft:'12px',marginTop:'15px'}}>
													<label className="label">Insert Type</label>
													<div className="tile" >
															<input
																className="input is-info"
																type="text"
																placeholder="Insert Type"
																value={this.state.name_type_technology}
																onChange={(e) => {this.setState({name_type_technology: e.target.value})}}
															/>
															<span
																className="button is-info is-outlined"
																style={{marginLeft:'1%'}}
																onClick={this.saveType.bind(this)}
															>Save Type</span>
													</div>
												</div>
											</div>
										</div>
									</article>

									<DataTable
										value={this.state.typeData}
										paginator={true}
										rows={8}
										selectionMode="single"
										style={{width:'90%'}}
										editable={true}
										emptyMessage="No records found">
										<Column
											field="name_type_technology"
											header="Name Type"
											editor={this.typeEditor.bind(this)}
											editorValidator={this.requiredValidator.bind(this)}
										/>
										<Column body={this.actionTemplate.bind(this)} style={{textAlign:'center', width: '90px'}}/>
									</DataTable>


								</div>
							</div>
						</div>
					</div>
				</div>

				<Growl ref={(el) => this.growl = el} />

			</div>
		)
	}
}
