import React,{Component} from 'react'
import axios from 'axios'
import {Dropdown} from 'primereact/dropdown'
import {Growl} from 'primereact/growl'
import Kind from './kind'
import Type from './type'
import {url_react,url_server} from '../../../service/url_part'

export default class TypeKind extends Component {
	constructor() {
		super();
		this.state={
			classNameType:'is-active',
			classNameKind:'',
			showTypeKind:<Type />

		}

	}
	componentDidMount() {

	}
	// change classname for click change type or kind
	changeClassName(e) {
		if(e == 'Type') {
			this.setState({
				classNameType: 'is-active',
				classNameKind: '',
				showTypeKind: <Type />
			})
		}
		else if (e == 'Kind') {
			this.setState({classNameKind: 'is-active',
			classNameType: '',
			showTypeKind: <Kind />
		})
		}
	}

	render() {
		return(
			<div className="box" style={{marginTop:'35px', marginRight:'1%', width:'70%'}}>
				<div class="tabs is-boxed">
					<ul>

						<li class={this.state.classNameType} onClick={this.changeClassName.bind(this,'Type')}>
							<a>
								<span class="icon is-small"><i class="fas fa-image" aria-hidden="true"></i></span>
								<span>Type</span>
							</a>
						</li>
						<li class={this.state.classNameKind} onClick={this.changeClassName.bind(this,'Kind')}>
							<a>
								<span class="icon is-small"><i class="fas fa-music" aria-hidden="true"></i></span>
								<span>Kind</span>
							</a>
						</li>

					</ul>
				</div>

				{this.state.showTypeKind}

			</div>

		)
	}
}
