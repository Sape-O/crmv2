import React,{Component} from 'react'
import axios from 'axios'
import {Dropdown} from 'primereact/dropdown'
import {Password} from 'primereact/password'
import {Growl} from 'primereact/growl'

import {url_react,url_server} from '../../service/url_part'

export default class AddMember extends Component {
	constructor() {
		super();
		this.state={
			_newUser:true,
			headName:'New User',
			userIDEdit:'',
			First_name:'',
			Last_name:'',
			Tel:'',
			Email:'',
			Username:'',
			Password:'',
			Permission:'',
			PermissionDropdown:[{label:'', value:''}]
		}
	}

	componentDidMount() {

		this.getPermission()

		if(this.props.newUser == false || this.props.userEdit != 0) {
			this.state._newUser = false
			this.state.userIDEdit = this.props.userEdit
			this.getDataForEdit()

		}

	}
	getDataForEdit() {

		axios.get(url_server + '/user/' + this.state.userIDEdit)
    .then(response => {
      if(response.data == "0")
        alert("Error query DB (react, getDataForEdit)")
      else{
				this.setState({
					First_name: response.data[0].First_name,
					Last_name: response.data[0].Last_name,
					Tel: response.data[0].Tel,
					Email: response.data[0].Email,
					Username: response.data[0].Username,
					Permission: response.data[0].Permission_id,
					headName: response.data[0].First_name
				})
			}
    })
    .catch(function (error) {
      alert('Error for query DB (catch react getDataForEdit)\n' + error)
    })
	}
	// get for select Premission
	getPermission() {

		let self = this
		axios.get(url_server + '/permission')
    .then(response => {
      if(response.data == "0")
        alert("Error query DB (react, getPermission)")
      else{
				let data = [...this.state.PermissionDropdown]
				for (var i = 0; i < response.data.length; i++) {
					data[i].value = response.data[i].Permission_Id
					data[i].label = response.data[i].Permission
					if(response.data.length-1 != i) {
						data=[...data, {label:'', value:''}]
					}
				}
				self.setState({PermissionDropdown: data})
			}
    })
    .catch(function (error) {
      alert('Error for query DB (catch react getPermission)\n' + error)
    })
	}

	clearState() {
		this.setState({
			First_name:'',
			Last_name:'',
			Tel:'',
			Email:'',
			Username:'',
			Password:'',
			Permission:''
		})
	}



	saveUser() {

		if(this.state.First_name == '' || this.state.First_name.length > 100)
			this.growl.show({severity: 'error', summary: 'Input data', detail: 'First name incorrect !'})
		else if (this.state.Last_name == '' || this.state.Last_name.length > 100)
			this.growl.show({severity: 'error', summary: 'Input data', detail: 'Last name incorrect !'})
		else if (this.state.Tel == '' || this.state.Tel.length > 11)
			this.growl.show({severity: 'error', summary: 'Input data', detail: 'Telephone incorrect !'})
		else if (this.state.Email == '' || this.state.Email.length > 100)
			this.growl.show({severity: 'error', summary: 'Input data', detail: 'Email incorrect !'})
		else if (this.state.Username == '' || this.state.Username.length > 100)
			this.growl.show({severity: 'error', summary: 'Input data', detail: 'Username incorrect !'})
		else if (this.state.Password == '' || this.state.Password.length > 100)
			this.growl.show({severity: 'error', summary: 'Input data', detail: 'Password incorrect !'})
			else if (this.state.Permission == '')
				this.growl.show({severity: 'error', summary: 'Input data', detail: 'Please Select Permission !'})
		else {

			let self = this

			if(this.state._newUser == true) {
				axios.post(url_server + '/management/newUser/', {
					First_name: this.state.First_name,
					Last_name: this.state.Last_name,
					Tel: this.state.Tel,
					Email: this.state.Email,
					Username: this.state.Username,
					Password: this.state.Password,
					Permission: this.state.Permission
					})
					.then(function (response) {
						if (response.data == "0")
							self.growl.show({severity: 'error', summary: 'Save', detail: 'Add User Fail !'})
						else if(response.data == "Success") {
							self.growl.show({severity: 'info', summary: 'Save', detail: 'Save '+response.data+' !'})
							self.clearState()
						}else alert(response.data)
					})
					.catch(function (error) {
						alert('Error for query DB (catch react saveUser(if true))\n' + error)
					})

			} else if(this.state._newUser == false) {
				axios.post(url_server + '/management/editUser/' + this.state.userIDEdit, {
					First_name: this.state.First_name,
					Last_name: this.state.Last_name,
					Tel: this.state.Tel,
					Email: this.state.Email,
					Username: this.state.Username,
					Password: this.state.Password,
					Permission: this.state.Permission
					})
					.then(function (response) {
						if (response.data == "0")
							self.growl.show({severity: 'error', summary: 'Edit User', detail: 'Edit Fail !'})
						else if(response.data == "Success") {
							alert('Edit '+response.data+' !')
							self.clearState()
							self.props.linkmember() // edit finish and turn to member
						}else alert(response.data)
					})
					.catch(function (error) {
						alert('Error for query DB (catch react (if false))\n' + error)
					})
			}
		}
	}

	render() {
		return(
			<div className="box" style={{marginTop:'35px', width:'70%'}}>
			<h3 class="title is-3">{this.state.headName}</h3>
				<div style={{marginLeft:'5%'}}>
					<div class="tile is-ancestor">
						<div class="tile is-vertical">
							<div class="tile">
								<div class="tile is-parent is-vertical">
									<article class="tile is-child  is-primary">
										<div class="tile is-ancestor">
											<div class="tile">
												<div className="field" style={{width:'90%', margin:'2%'}}>
													<label className="label">First Name</label>
													<div className="control" >
														<input
															className="input is-info"
															type="text"
															placeholder="First Name"
															value={this.state.First_name}
															onChange={(e) => {this.setState({First_name: e.target.value})}}
														/>
													</div>
												</div>
											</div>
											<div class="tile">
												<div className="field" style={{width:'90%', margin:'2%'}}>
													<label className="label">Last Name</label>
													<div className="control" >
														<input
															className="input is-info"
															type="text"
															placeholder="Last Name"
															value={this.state.Last_name}
															onChange={(e) => {this.setState({Last_name: e.target.value})}}
														/>
													</div>
												</div>
											</div>
										</div>
									</article>
									<article class="tile is-child  is-warning">
										<div class="tile is-ancestor">
											<div class="tile">
												<div className="field" style={{width:'90%', margin:'2%'}}>
													<label className="label">Telephone</label>
													<div className="control" >
														<input
															className="input is-primary"
															type="text"
															placeholder="Telephone"
															value={this.state.Tel}
															onChange={(e) => {this.setState({Tel: e.target.value})}}
														/>
													</div>
												</div>
											</div>
											<div class="tile">
												<div className="field" style={{width:'90%', margin:'2%'}}>
													<label className="label">Email</label>
													<div className="control" >
														<input
															className="input is-primary"
															type="text"
															placeholder="Email"
															value={this.state.Email}
															onChange={(e) => {this.setState({Email: e.target.value})}}
														/>
													</div>
												</div>
											</div>
										</div>
									</article>
									<article class="tile is-child  is-warning">
										<div class="tile is-ancestor">
											<div class="tile">
												<div className="field" style={{width:'90%', margin:'2%'}}>
													<label className="label">Username</label>
													<div className="control" >
														<input
															className="input is-info"
															type="text"
															placeholder="Username"
															value={this.state.Username}
															onChange={(e) => {this.setState({Username: e.target.value})}}
														/>
													</div>
												</div>
											</div>
											<div class="tile">
												<div className="field" style={{width:'90%', margin:'2%'}}>
													<label className="label">Password</label>
													<div className="control" >
														<Password
															className="input is-warning"
															type="password"
															placeholder="Password"
															value={this.state.Password}
															onChange={(e) => {this.setState({Password: e.target.value})}}
														/>
													</div>
												</div>
											</div>
										</div>
									</article>
									<article class="tile is-child  is-warning">
										<div class="tile is-ancestor">
											<div class="tile">
												<div className="field" style={{width:'90%', margin:'1%'}}>
													<label className="label">Permission</label>
													<Dropdown
														value={this.state.Permission}
														options={this.state.PermissionDropdown}
														onChange={(e) => {this.setState({Permission: e.target.value})}}
														placeholder="Select a Permission"
														style={{width:'50%'}}
													/>
												</div>
											</div>
										</div>
									</article>
									<article class="tile is-child  is-warning">
										<div class="tile is-ancestor">
											<div class="tile">
												<div className="field" style={{width:'90%', margin:'2%'}}>
													<div className="control" >
														<a
															class="button is-info is-focused"
															style={{width:'100%'}}
															onClick={this.saveUser.bind(this)}
														>Save</a>
													</div>
												</div>
											</div>
											<div class="tile">
												<div className="field" style={{width:'90%', margin:'2%'}}>
													<div className="control" >
														{this.state._newUser == true ?
															<a class="button is-danger is-focused" style={{width:'100%'}}  onClick={this.clearState.bind(this)}>Clear</a>
															:
															<a class="button is-danger is-focused" style={{width:'100%'}}  disabled>Clear</a>
														}
													</div>
												</div>
											</div>
										</div>
									</article>
								</div>
							</div>
						</div>
					</div>
				</div>

				<Growl ref={(el) => this.growl = el} />

			</div>

		)
	}
}
