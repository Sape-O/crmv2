import React,{Component} from 'react'
import axios from 'axios'
import {Dropdown} from 'primereact/dropdown'
import {Password} from 'primereact/password'
import {Growl} from 'primereact/growl'

import {url_react,url_server} from '../../service/url_part'

export default class AddVendor extends Component {
	constructor() {
		super();
		this.state={
			_newVendor:true,
			headName:'New Vendor',
			vendorIDEdit:'',
			NameProduct:'',
			Name_vendor:'',
			Tel_vendor:'',
			Email_vendor:''
		}
	}

	componentDidMount() {

		if(this.props.newVendor == false || this.props.vendorEdit != 0) {
			this.state._newVendor = false
			this.state.vendorIDEdit = this.props.vendorEdit
			this.getDataForEdit()
		}

	}
	getDataForEdit() {
		let self = this
		axios.get(url_server + '/vendor/' + this.state.vendorIDEdit)
    .then(response => {
      if(response.data == "0")
        alert("Error query Vendor (react, getDataForEdit(if))")
      else{
				self.setState({
					NameProduct: response.data[0].NameProduct,
					Name_vendor: response.data[0].Name_vendor,
					Tel_vendor: response.data[0].Tel_vendor,
					Email_vendor: response.data[0].Email_vendor,
					headName: response.data[0].Name_vendor
				})
			}
    })
    .catch(function (error) {
      alert('Error for query DB (catch react getDataForEdit)\n' + error)
    })
	}

	clearState() {
		this.setState({
			NameProduct:'',
			Name_vendor:'',
			Tel_vendor:'',
			Email_vendor:''
		})
	}

	saveVendor() {

		if(this.state.NameProduct == '' || this.state.NameProduct.length > 100)
			this.growl.show({severity: 'error', summary: 'Input data', detail: 'Name Product incorrect !'})
		else if (this.state.Name_vendor == '' || this.state.Name_vendor.length > 100)
			this.growl.show({severity: 'error', summary: 'Input data', detail: 'Name Vendor incorrect !'})
		else if (this.state.Tel_vendor == '' || this.state.Tel_vendor.length > 11)
			this.growl.show({severity: 'error', summary: 'Input data', detail: 'Telephone incorrect !'})
		else if (this.state.Email_vendor == '' || this.state.Email_vendor.length > 100)
			this.growl.show({severity: 'error', summary: 'Input data', detail: 'Email incorrect !'})
		else {

			let self = this

			if(this.state._newVendor == true) {
				axios.post(url_server + '/management/newVendor/', {
					NameProduct: this.state.NameProduct,
					Name_vendor: this.state.Name_vendor,
					Tel_vendor: this.state.Tel_vendor,
					Email_vendor: this.state.Email_vendor
				})
				.then(function (response) {
					if (response.data == "0")
						self.growl.show({severity: 'error', summary: 'Save', detail: 'Add Vendor Fail !'})
					else if(response.data == "Success") {
						self.growl.show({severity: 'info', summary: 'Save', detail: 'Save '+response.data+' !'})
						self.clearState()
					}else alert(response.data)
				})
				.catch(function (error) {
					alert('Error for query DB (catch react saveUser(if true))\n' + error)
				})

			} else if(this.state._newVendor == false) {
				axios.post(url_server + '/management/editVendor/' + this.state.vendorIDEdit, {
					NameProduct: this.state.NameProduct,
					Name_vendor: this.state.Name_vendor,
					Tel_vendor: this.state.Tel_vendor,
					Email_vendor: this.state.Email_vendor
				})
				.then(function (response) {
					if (response.data == "0")
						self.growl.show({severity: 'error', summary: 'Edit Vendor', detail: 'Edit Fail !'})
					else if(response.data == "Success") {
						alert('Edit '+response.data+' !')
						self.clearState()
						self.props.linkvendor()
					}else alert(response.data)
				})
				.catch(function (error) {
					alert('Error for query DB (catch react (if false))\n' + error)
				})
			}
		}
	}

	render() {
		return(
			<div className="box" style={{marginTop:'35px', width:'70%'}}>
			<h3 className="title is-3">{this.state.headName}</h3>
				<div style={{marginLeft:'5%'}}>
					<div className="tile is-ancestor">
						<div className="tile is-vertical">
							<div className="tile">
								<div className="tile is-parent is-vertical">
									<article className="tile is-child  is-primary">
										<div className="tile is-ancestor">
											<div className="tile">
												<div className="field" style={{width:'90%', margin:'2%'}}>
													<label className="label">Name Product</label>
													<div className="control" >
														<input
															className="input is-info"
															type="text"
															placeholder="First Name"
															value={this.state.NameProduct}
															onChange={(e) => {this.setState({NameProduct: e.target.value})}}
														/>
													</div>
												</div>
											</div>
											<div className="tile">
												<div className="field" style={{width:'90%', margin:'2%'}}>
													<label className="label">Name Vendor</label>
													<div className="control" >
														<input
															className="input is-info"
															type="text"
															placeholder="Last Name"
															value={this.state.Name_vendor}
															onChange={(e) => {this.setState({Name_vendor: e.target.value})}}
														/>
													</div>
												</div>
											</div>
										</div>
									</article>
									<article className="tile is-child  is-warning">
										<div className="tile is-ancestor">
											<div className="tile">
												<div className="field" style={{width:'90%', margin:'2%'}}>
													<label className="label">Telephone</label>
													<div className="control" >
														<input
															className="input is-primary"
															type="text"
															placeholder="Telephone"
															value={this.state.Tel_vendor}
															onChange={(e) => {this.setState({Tel_vendor: e.target.value})}}
														/>
													</div>
												</div>
											</div>
											<div className="tile">
												<div className="field" style={{width:'90%', margin:'2%'}}>
													<label className="label">Email</label>
													<div className="control" >
														<input
															className="input is-primary"
															type="text"
															placeholder="Email"
															value={this.state.Email_vendor}
															onChange={(e) => {this.setState({Email_vendor: e.target.value})}}
														/>
													</div>
												</div>
											</div>
										</div>
									</article>
									<article className="tile is-child  is-warning">
										<div className="tile is-ancestor">
											<div className="tile">
												<div className="field" style={{width:'90%', margin:'2%'}}>
													<div className="control" >
														<a
															className="button is-info is-focused"
															style={{width:'100%'}}
															onClick={this.saveVendor.bind(this)}
														>Save</a>
													</div>
												</div>
											</div>
											<div className="tile">
												<div className="field" style={{width:'90%', margin:'2%'}}>
													<div className="control" >
														{this.state._newVendor == true ?
															<a className="button is-danger is-focused" style={{width:'100%'}}  onClick={this.clearState.bind(this)}>Clear</a>
															:
															<a className="button is-danger is-focused" style={{width:'100%'}}  disabled>Clear</a>
														}
													</div>
												</div>
											</div>
										</div>
									</article>
								</div>
							</div>
						</div>
					</div>
				</div>

				<Growl ref={(el) => this.growl = el} />

			</div>

		)
	}
}
