import React, { Component } from 'react'
import axios from 'axios'
import {Link} from 'react-router-dom'
import {Button} from 'primereact/button'
import {DataTable} from 'primereact/datatable'
import {Column} from 'primereact/column'
import {InputText} from 'primereact/inputtext'
import {Dialog} from 'primereact/dialog'
import {Growl} from 'primereact/growl'

import {url_react,url_server} from '../../service/url_part'

export default class Customer extends Component {
  constructor() {
        super();
        this.state = {
          visible: false,
          search:'',
          customersData:[],
          Customer_Id:'',
          Name_company:'',
          Address_company:'',
          Name_contact:'',
          Position_contact:'',
          Email_contact:'',
          Tel_desk:'',
          Tel_mobile:''
        }

  }

  getCustomer() {
    axios.get(url_server + '/customers')
    .then(response => {
      if(response.data == "0")
        alert("Error query Customer (react, getCustomer)")
      else
        this.setState({ customersData: response.data })
    })
    .catch(function (error) {
      alert('Error for query DB (catch react)\n' + error)
    })
  }
  componentDidMount() {
    this.getCustomer()
  }

  // export data to CSV not use in this time
  export() {
    this.dt.exportCSV()
  }
  // for Dialog

  onHide() {
    /*
     * เมื่อ Dialog ปิดตัวลง ให้ทำการ ล้างค้าที่อยู่ใน state ออกให้หมด
     * ป้องกัน new customer แล้วมีค่าค้าง
     */
    this.setState({visible: false})
    this.clearState()
  }

  saveCustomer() {

    if(this.state.Name_company == '' || this.state.Name_company.length >=100)
      this.growl.show({severity: 'error', summary: 'Error Message', detail: 'Name company incorrect !'})
    else if (this.state.Address_company == '' || this.state.Address_company.length >= 100)
      this.growl.show({severity: 'error', summary: 'Error Message', detail: 'Address company incorrect !'})
    else if (this.state.Name_contact == '' || this.state.Name_contact.length >= 100)
      this.growl.show({severity: 'error', summary: 'Error Message', detail: 'Name contact incorrect !'})
    else if (this.state.Position_contact == '' || this.state.Position_contact.length >= 100)
      this.growl.show({severity: 'error', summary: 'Error Message', detail: 'Position contact incorrect !'})
    else if (this.state.Email_contact == '' || this.state.Email_contact.length >= 100)
      this.growl.show({severity: 'error', summary: 'Error Message', detail: 'Email contact incorrect !'})
    else if (this.state.Tel_desk == '' || this.state.Tel_desk.length >= 16)
      this.growl.show({severity: 'error', summary: 'Error Message', detail: 'Tel desk incorrect !'})
    else if (this.state.Tel_mobile == '' || this.state.Tel_mobile.length >= 11)
      this.growl.show({severity: 'error', summary: 'Error Message', detail: 'Tel mobile incorrect !'})
    else {

      let self = this

      if(this._newCustomer === true) {

        axios.post(url_server + '/customers/newcustomer', {
          Customer_Id: this.state.Customer_Id,
          Name_company: this.state.Name_company,
          Address_company: this.state.Address_company,
          Name_contact: this.state.Name_contact,
          Position_contact: this.state.Position_contact,
          Email_contact: this.state.Email_contact,
          Tel_desk: this.state.Tel_desk,
          Tel_mobile: this.state.Tel_mobile
        })
        .then(function (response) {

          // ปกติ response คือ success ถ้า success ให้ ปิด Dialog
          // ถ้าไม่ ให้แสดง error ที่ส่งจาก Backend (nodejs)
          if (response.data == "Success"){
            self.clearState()
            self.setState({visible: false})
            self.getCustomer() // update data in table auto update
            alert(response.data)
          } else
            alert(response.data)

        })
        .catch(function (error) {
          alert('Error for query DB (catch react)\n' + error)
        })


      } else if(this._newCustomer === false) {

        axios.post(url_server + '/customer/edit', {
          Customer_Id:this.state.Customer_Id,
          Name_company: this.state.Name_company,
          Address_company: this.state.Address_company,
          Name_contact: this.state.Name_contact,
          Position_contact: this.state.Position_contact,
          Email_contact: this.state.Email_contact,
          Tel_desk: this.state.Tel_desk,
          Tel_mobile: this.state.Tel_mobile
        })
        .then(function (response) {
          if (response.data == "Update Success"){
            self.clearState()
            self.setState({visible: false})
            self.getCustomer() // update data in table auto update
            alert(response.data)
          } else {
            alert(response.data)
          }
        })
        .catch(function (error) {
          alert('Error for query DB (catch react)\n' + error)
        })
      }
    }
  }

  clearState() {
    this.setState({
        Customer_Id:'',
        Name_company:'',
        Address_company:'',
        Name_contact:'',
        Position_contact:'',
        Email_contact:'',
        Tel_desk:'',
        Tel_mobile:''
      })
  }

  newCustomer() {
    this._newCustomer = true
    this.setState({visible: true})
  }

  editButton(e) {
    this._newCustomer = false
    this.setState({visible: true})

    this.setState({
      Customer_Id: e.Customer_Id,
      Name_company: e.Name_company,
      Address_company: e.Address_company,
      Name_contact: e.Name_contact,
      Position_contact: e.Position_contact,
      Email_contact: e.Email_contact,
      Tel_desk: e.Tel_desk,
      Tel_mobile: e.Tel_mobile
    })
  }

  actionTemplate(rowData) {
    return( <div>
              <span
                type="button"
                className="button is-warning is-focused"
                style={{width: '40px',marginRight:'5px'}}
                onClick={this.editButton.bind(this,rowData)}
              >Edit</span>
              <Link to={'/Contact/' + rowData['Customer_Id']}>
                <span
                  type="button"
                  className="button is-primary is-focused"
                  style={{width: '70px',marginRight:'5px'}}
                >Contact</span>
              </Link>
              <Link to={'/Project/' + rowData['Customer_Id']}>
                <span
                  type="button"
                  className="button is-info is-focused"
                  style={{width: '65px'}}
                >Project</span>
              </Link>
            </div>
          )
    }

  render() {
    var header =<div>
                  <div style={{'textAlign':'left'}}>
                    <i className="pi pi-search" style={{margin:'4px 4px 0 0'}} />
                    <InputText
                      type="search"
                      placeholder="Global Search"
                      size="50"
                      onInput={(e) => this.setState({globalFilter: e.target.value})}
                    />
                  </div>
                  <div style={{'textAlign':'right'}}>
                    <Button
                      disabled
                      type="button"
                      icon="pi pi-external-link"
                      iconPos="left"
                      label="Export CSV"
                      style={{'textAlign':'right'}}
                      onClick={this.export.bind(this)}
                    />
                  </div>
                </div>

    const footer = (
            <div>

                <a class="button is-info is-outlined" style={{marginRight:'1%'}}>
                  <span class="icon is-small">
                    <i class="fas fa-check"></i>
                  </span>
                  <span style={{width:'100px'}} onClick={this.saveCustomer.bind(this)}>
                    {this.state.Customer_Id? "Update":"Save"}
                  </span>
                </a>

                <a class="button is-danger is-outlined" style={{marginLeft:'1%'}}>
                  <span style={{width:'100px'}} onClick={this.onHide.bind(this)}>Cancel</span>
                  <span class="icon is-small">
                    <i class="fas fa-times"></i>
                  </span>
                </a>

            </div>
        )




    return(
      <div>

              <div className="">

                <div>

                  <span
                    className="btn btn-secondary"
                    onClick={this.newCustomer.bind(this)}
                    style={{width: '150px',marginBottom:'20px',marginTop:'20px'}}
                  >New Customer</span>

                  <Dialog
                    header={this.state.Customer_Id ? "Edit Customer":"New Customer"}
                    footer={footer}
                    visible={this.state.visible}
                    style={{width: '50vw'}}
                    modal={true}
                    onHide={this.onHide.bind(this)}
                  >
                    <div>
                      <div className="tile is-ancestor">
                        <div className="tile is-vertical">
                          <div className="tile">

                            <div className="tile is-parent is-vertical">
                              <div className="field">
                                <label className="label">Name Company </label>
                                <div className="control">
                                  <input
                                    className="input is-primary"
                                    type="text"
                                    placeholder="Name Company"
                                    style={{width:'340px'}}
                                    value={this.state.Name_company}
                                    onChange={(e) => this.setState({Name_company:e.target.value})}
                                  />
                                </div>
                              </div>
                              <div className="field">
                                <label className="label">Name Contact</label>
                                <div className="control">
                                  <input
                                    className="input is-primary"
                                    type="text"
                                    placeholder="Name Contact"
                                    style={{width:'340px'}}
                                    value={this.state.Name_contact}
                                    onChange={(e) => this.setState({Name_contact:e.target.value})}
                                  />
                                </div>
                              </div>
                            </div>
                            <div className="tile is-parent">
                              <div className="field">
                                <label className="label">Address Company</label>
                                <div className="control">
                                  <textarea
                                    className="textarea"
                                    placeholder="Address Company"
                                    style={{width:'340px'}}
                                    value={this.state.Address_company}
                                    onChange={(e) => this.setState({Address_company:e.target.value})}
                                  />
                                </div>
                              </div>
                            </div>
                          </div>

                          <div className="tile">
                            <div className="tile is-parent">
                              <div className="field">
                                <label className="label">Position</label>
                                <div className="control">
                                  <input
                                    className="input is-primary"
                                    type="text"
                                    placeholder="Position"
                                    style={{width:'340px'}}
                                    value={this.state.Position_contact}
                                    onChange={(e) => this.setState({Position_contact:e.target.value})}
                                  />
                                </div>
                              </div>
                            </div>
                            <div className="tile is-parent">
                              <div className="field">
                                <label className="label">Email</label>
                                <div className="control">
                                  <input
                                    className="input is-primary"
                                    type="text"
                                    placeholder="patiphan@nccs.co.th"
                                    style={{width:'340px'}}
                                    value={this.state.Email_contact}
                                    onChange={(e) => this.setState({Email_contact:e.target.value})}
                                  />
                                </div>
                              </div>
                            </div>
                          </div>

                          <div className="tile">
                            <div className="tile is-parent">
                              <div className="field">
                                <label className="label">Tel Desk</label>
                                <div className="control">
                                  <input
                                    className="input is-primary"
                                    type="text"
                                    placeholder="02-999-9999"
                                    style={{width:'340px'}}
                                    value={this.state.Tel_desk}
                                    onChange={(e) => this.setState({Tel_desk:e.target.value})}
                                  />
                                </div>
                              </div>
                            </div>
                            <div className="tile is-parent">
                              <div className="field">
                                <label className="label">Tel Mobile</label>
                                <div className="control">
                                  <input
                                    className="input is-primary"
                                    type="text"
                                    placeholder="085-643-2941"
                                    style={{width:'340px'}}
                                    value={this.state.Tel_mobile}
                                    onChange={(e) => this.setState({Tel_mobile:e.target.value})}
                                  />
                                </div>
                              </div>
                            </div>
                          </div>

                        </div>
                      </div>
                    </div>
                  </Dialog>
                </div>

                <div className="content-section implementation">
                  <DataTable
                    value={this.state.customersData}
                    paginator={true}
                    rows={30} rowsPerPageOptions={[30,50,80,100,300,500,1000]}
                    header={header}
                    globalFilter={this.state.globalFilter}
                    selectionMode="single"
                    emptyMessage="No records found">
                    <Column field="Name_company" header="Name Company" sortable={true} />
                    <Column field="Address_company" header="Address Company" />
                    <Column field="Name_contact" header="Contact" />
                    <Column field="Position_contact" header="Position" />
                    <Column field="Email_contact" header="Email" />
                    <Column field="Tel_desk" header="Tel Desk" />
                    <Column field="Tel_mobile" header="Tel Mobile" />
                    <Column body={this.actionTemplate.bind(this)} style={{textAlign:'center', width: '230px'}}/>
                  </DataTable>
                </div>

                <Growl ref={(el) => this.growl = el} />

              </div>
      </div>
    )

  }

}
