import React,{Component} from 'react'
import {Password} from 'primereact/password'
import {Button} from 'primereact/button'
import {Growl} from 'primereact/growl'
import axios from 'axios'

import {url_react,url_server} from '../../service/url_part'


 export default class Login extends Component {
   constructor() {
    super();
    this.state={
      username:'',
      password:'',
      countLogin:0
    }
  }

  forget() {
    this.growl.show({severity: 'error', summary: 'Error Login', detail: 'บอกพี่ก้องนู้น ปุ่มนี้ใช้ไม่ได้ !'})
  }

  apiLogin(username, password) {
    axios.post(url_server + '/login', {
      Username: username,
      Password: password
    })
    .then(response => {
      if(response.data == "0") {
        this.setState({countLogin: this.state.countLogin + 1})
        if(this.state.countLogin >= 3)
          this.growl.show({severity: 'error', summary: 'Error Login', detail: 'ความจำสั้นจัง Username password ก็จำไม่ได้ !'})
        else
          this.growl.show({severity: 'error', summary: 'Error Login', detail: 'Username or Password ไม่ถูกต้อง !'})
      }else {
        // save to local starage
        if (typeof(Storage) !== "undefined") {
          localStorage.clear()
          // Store
          localStorage.setItem('User_Id', response.data[0].User_Id)
          localStorage.setItem('Last_name', response.data[0].Last_name)
          localStorage.setItem('Tel', response.data[0].Tel)
          localStorage.setItem('Email', response.data[0].Email)
          localStorage.setItem('Permission_id', response.data[0].Permission_id)
          localStorage.setItem('Username', response.data[0].Username)
          // Retrieve
          window.location.href = url_react
        } else {
          this.growl.show({severity: 'error', summary: 'Error Message', detail: 'Can\'t SET localStorage!'})
        }

      }
    })
    .catch(function (error) {
      alert('Error for query DB (catch react)\n' + error)
    })
  }

  login() {
    if(this.state.username=='') {
      this.growl.show({severity: 'warn', summary: 'Error Login', detail: 'กรุณากรอก Username !'})
    }else if(this.state.password=='') {
      this.growl.show({severity: 'warn', summary: 'Error Login', detail: 'กรุณากรอก Password !'})
    }else {
      this.apiLogin(this.state.username, this.state.password)
    }
  }

  render() {

      return(
          <div className="box" style={{width:'60%',marginLeft:'20%',marginTop:'5%',marginBottom:'10%'}}>
            <div className="tile is-ancestor" >
              <div className="tile is-vertical is-7">
                <div className="tile is-parent">
                  <article className="tile is-child ">
                    <div className="content" style={{margin:"30px"}}>
                      <img src="logoPNG.png"/>
                    </div>
                  </article>
                </div>
              </div>
              <div className="tile is-parent">
                <div className="tile is-child">
                  <div className="content " style={{margin:"30px"}}>
                      <div className="box " style={{width:"330px"}} >
                      <p>Log in</p>
                        <div className="field" >
                          <label className="label">Username</label>
                          <div className="control has-icons-left has-icons-right">
                            <input
                              className="input is-success"
                              type="text"
                              placeholder="Username"
                              value={this.state.username}
                              onChange={(e) => this.setState({username: e.target.value})}
                            />
                          </div>
                        </div>
                        <div className="field" >
                          <label className="label">Password</label>
                          <div className="control has-icons-left has-icons-right">
                            <Password
                              className="input is-success"
                              placeholder="Password"
                              value={this.state.password}
                              onChange={(e) => this.setState({password: e.target.value})}
                            />
                          </div>
                        </div>
                        <div className="field" >
                          <div className="control has-icons-left">
                            <a
                            style={{color:"red"}}
                            onClick={this.forget.bind(this)}
                            >Forget Password</a>
                          </div>
                          <br />
                          <div className="control has-icons-left">
                            <Button
                              style={{width:"40%"}}
                              label="Sign in"
                              className="p-button-raised"
                              onClick={this.login.bind(this)}
                            />
                          </div>
                        </div>
                      </div>
                  </div>
                </div>
              </div>
            </div>

            <Growl ref={(el) => this.growl = el} />

          </div>
      )
  }
}
