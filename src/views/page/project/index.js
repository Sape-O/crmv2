import React, { Component } from 'react'
import axios from 'axios'
import {Button} from 'primereact/button'
import {DataTable} from 'primereact/datatable'
import {Column} from 'primereact/column'
import {InputText} from 'primereact/inputtext'
import {Link} from 'react-router-dom'
import {Dialog} from 'primereact/dialog'
import {url_react,url_server} from '../../service/url_part'
import {formatNumber} from './../../check/check_input_data'


export default class Project extends Component {
  constructor() {
        super();
        this.state = {
					projectData:[],
          typeTechData:'',
          Customer_Id:'',
          listData:[]
        }
        this.rowExpansionTemplate = this.rowExpansionTemplate.bind(this)

  }

  componentDidMount() {

    let id  = this.props.match.params.id
    this.setState({Customer_Id: id})
    let self = this
    axios.get(url_server + '/project/' + id)
      .then(response => {
        if(response.data == "0")
          alert("Error query Project (react, componentDidMount)")
        else {
          let t = [...response.data]
          for(var i = 0;i < response.data.length; i++) {
            t[i].Amount = formatNumber(response.data[i].Amount)
          }
          self.setState({projectData: t})
        }
      })
      .catch(function (error) {
        alert('Error for query DB (catch react)\n' + error)
      })
  }

  //--------------GET DATA To show List--------//
  getList(e) {
    axios.get(url_server + '/project/edit/' + e)
        .then(response => {
          if(response.data == "0")
            alert("Error query Project (react, getList)")
          else
            this.setState({listData: response.data})
        })
        .catch(function (error) {
          alert('Error for query DB (catch react)\n' + error)
        })
  }
  //----------- End GET DATA To show List--------//

  rowExpansionTemplate(data) {
    this.getList(data.Project_Id)

      return  (
          <div className="p-grid p-fluid" style={{padding: '2em 1em 1em 1em'}}>
              <div style={{textAlign:'center'}}>
              <div>
                <table className="table">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Technology</th>
                      <th>Kind</th>
                      <th>Name Vendor</th>
                      <th>Email Vendor</th>
                    </tr>
                  </thead>
                  <tbody>
                    {
                      this.state.listData.map((item, idx) => {
                        return (
                          <tr>
                            <th>{idx+1}</th>
                            <td>{item.name_type_technology}</td>
                            <td>{item.Name_kind_type_tech}</td>
                            <td>{item.Name_vendor}</td>
                            <td>{item.Email_vendor}</td>
                          </tr>
                        )
                      })
                    }
                  </tbody>
                </table>
              </div>
            </div>
          </div>
      )
  }
  actionTemplate(rowData) {
        return <div>
                  <Link to={'/Project/' + rowData['Customer_id'] + '/' + rowData['Project_Id']}>
                    <button
                      type="button"
                      className="button is-warning is-focused"
                      style={{width: '40px', margin:'3px'}}
                    >Edit</button>
                  </Link>
                    <button
                      disabled
                      type="button"
                      className="button is-danger is-focused"
                      style={{width: '70px', margin:'3px'}}
                    >Delete</button>

              </div>
    }
  render() {
    var header =<div>
                  <div style={{'textAlign':'left'}}>
                    <i className="pi pi-search" style={{margin:'4px 4px 0 0'}} />
                    <InputText
                      type="search"
                      placeholder="Global Search"
                      size="50"
                      onInput={(e) => this.setState({globalFilter: e.target.value})}
                    />
                  </div>
                </div>

    return(
            <div className="box" style={{marginTop:"10px",marginLeft:"6px",marginRight:"6px"}}>
              <p className="title is-1">Project</p>

              <div>
                <Link to={'/Project/'+this.state.Customer_Id+'/New_project'}>
                  <span
                    className="button is-info is-medium is-outlined"
                    style={{marginBottom:'20px',marginTop:'20px'}}
                  >New Project</span>
                </Link>
              </div>

              <div className="content-section implementation">
                  <DataTable
                    value={this.state.projectData}
                    header={header}
                    expandedRows={this.state.expandedRows}
                    onRowToggle={(e) => this.setState({expandedRows:e.data})}
                    rowExpansionTemplate={this.rowExpansionTemplate}
                    globalFilter={this.state.globalFilter}
                    selectionMode="single"
                    emptyMessage="No records found">
                    <Column expander={true} style={{width: '2px'}} />
                    <Column field="NameProject" header="Name Project" sortable={true} filter={true} />
                    <Column field="Deal_status" header="Deal status"  filter={true} />
                    <Column field="Estimate" header="Estimate"  filter={true} />
                    <Column field="Amount" header="Amount"  filter={true} />
                    <Column body={this.actionTemplate.bind(this)} style={{textAlign:'center', width: '150px'}}/>
                  </DataTable>
              </div>
            </div>
          )

  }

}
