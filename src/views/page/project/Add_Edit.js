import React, { Component } from 'react'
import axios from 'axios'
import {Button} from 'primereact/button'
import {InputText} from 'primereact/inputtext'
import {Calendar} from 'primereact/calendar';
import {Dropdown} from 'primereact/dropdown';
import {Growl} from 'primereact/growl';
import {formatNumber, onlyNumber, spritCommaNumber} from './../../check/check_input_data'
import {url_react,url_server} from '../../service/url_part'

export default class Add_Edit extends Component {
  constructor() {
        super();
        this.state = {
          nameProject:'',
          amount:'',
          dealStatus:'',
          estimate:'',
          technology:[{type:'', kind:'', vendor:''}],
          _newProject:'',
          type:[{label:'', value:''}],
          kind:[],
          vendor:[{label:'', value:''}],
          project_id_Send:[]
        }
  }
  componentDidMount() {
    /*
     * get DB leady for select of type , vendor
     */
    let self = this
    axios.get(url_server + '/type_technology')
    .then(response => {
      if(response.data == "0")
        alert("Error query Project (react, componentDidMount)")
      else {

        let data = [...this.state.type]
        for (var i = 0; i < response.data.length; i++) {
          data[i].value = response.data[i].Type_technology_Id
          data[i].label = response.data[i].name_type_technology
          if(response.data.length-1 != i) {
            data=[...data, {label:'', value:''}]
          }
        }
        self.setState({type:data})

      }
    })
    .catch(function (error) {
      alert('Error for query DB (catch react, /type_technology)\n' + error)
    })

    axios.get(url_server + '/vendor')
    .then(response => {
      if(response.data == "0")
        alert("Error query Project (react, componentDidMount)")
      else {

        let data = [...this.state.vendor]
        for (var i = 0; i < response.data.length; i++) {
          data[i].value = response.data[i].Vendor_Id
          data[i].label = response.data[i].Name_vendor + " (" + response.data[i].NameProduct + ")"
          if(response.data.length-1 != i) {
            data=[...data, {label:'', value:''}]
          }
        }
        self.setState({vendor:data})

      }
    })
    .catch(function (error) {
      alert('Error for query DB (catch react, /vendor)\n' + error)
    })
    //------------------ end get DB type , vendor----------//

    // id1 = Customer_id
    // id2 = project_id if new project is newProject not id of project
    const id1  = this.props.match.params.id1
    const id2  = this.props.match.params.id2

    if(id2 === "New_project"){
      self.setState({_newProject : true})
    }else {
      self.setState({_newProject : false})

      axios.get(url_server + '/project/edit/' + id2)
      .then(response => {

        self.setState({nameProject: response.data[0].NameProject,
          amount: formatNumber(response.data[0].Amount),
          dealStatus: response.data[0].Deal_status,
          estimate: new Date(response.data[0].Estimate)
        })

        let data = [...this.state.technology]
        let project_id_SendTemp = []

        for (var i=0;i<response.data.length;i++) {
          project_id_SendTemp[i] = response.data[i].Project_type_Id
          data[i].type = response.data[i].Type_technology_Id
          data[i].kind = response.data[i].Kind_type_tech_Id
          data[i].vendor = response.data[i].Vendor_Id

          self.getKind_type_tech(i,response.data[i].Type_technology_Id)

          if(response.data.length-1 != i) {
            data=[...data, {type:'', kind:'', vendor:''}]
          }
        }

        self.setState({technology:data})
        self.setState({project_id_Send:project_id_SendTemp})
    })
      .catch(function (error) {
        alert('Error for query DB (catch react, /edit)\n' + error)
      })
    }

  }

  saveProject() {

    if(this.state.nameProject == '' || this.state.nameProject.length >= 100)
      this.growl.show({severity: 'error', summary: 'Error Message', detail: 'Name Project incorrect !'})
    else if(this.state.amount == '' || this.state.amount.length >= 20)
      this.growl.show({severity: 'error', summary: 'Error Message', detail: 'amount incorrect !'})
    else if(this.state.dealStatus == '' || this.state.dealStatus.length >= 100)
      this.growl.show({severity: 'error', summary: 'Error Message', detail: 'Deal Status incorrect !'})
    else if(this.state.estimate == '' || this.state.estimate.length >= 100)
      this.growl.show({severity: 'error', summary: 'Error Message', detail: 'Estimate incorrect !'})
    else if(this.state.technology == [{type:'', kind:'', vendor:''}])
      this.growl.show({severity: 'error', summary: 'Error Message', detail: 'technology incorrect !'})
    else {

      if(this.state._newProject === true) {

        let self = this
        axios.post(url_server + '/project/newproject/' + this.props.match.params.id1, {
            nameProject: this.state.nameProject,
            amount: spritCommaNumber(this.state.amount),
            dealStatus: this.state.dealStatus,
            estimate: this.state.estimate,
            technology: this.state.technology
          })
        .then(function (response) {

          if (response.data == "0")
            alert("Error query Project (react, saveProject)")
          else if(response.data == "Success") {

            alert(response.data)
            self.setState({nameProject: '',
                           amount: '',
                           dealStatus: '',
                           estimate: '',
                           technology: [{type:'', kind:'', vendor:''}]
                         })
            self.back()

          }else alert(response.data)

        })
        .catch(function (error) {
          alert('Error for query DB (catch react)\n' + error)
        })
      }else if(this.state._newProject === false) {

        let self = this
        axios.post(url_server + '/project/edit/' + this.props.match.params.id2, {
            nameProject: this.state.nameProject,
            amount: spritCommaNumber(this.state.amount),
            dealStatus: this.state.dealStatus,
            estimate: this.state.estimate,
            technology: this.state.technology,
            project_id_Send:this.state.project_id_Send
          })
          .then(function (response) {

            if (response.data == "Success") {

              alert(response.data)
              self.setState({nameProject: '',
                            amount: '',
                            dealStatus: '',
                            estimate: '',
                            technology: [{type:'', kind:'', vendor:''}]
                          })
              self.back()

            } else {
              alert(response.data)
            }
          })
          .catch(function (error) {
            alert('Error for query DB (catch react)\n' + error)
          })
      }
    }

  }
  back() {
    // ชั่วคราว ยังไม่มีเวลา ทำแบบ props กลับไป page เดิม refrash ไปก่อน
    window.location.href = url_react + '/Project/' + this.props.match.params.id1
  }

  add() {
    this.addSelect()
  }
  addSelect = (e) => {
    this.setState((prevState) => ({
      technology: [...prevState.technology, {type:'', kind:'', vendor:''}],
    }))
  }

  deleteSelect = (e) => {

    let data = [...this.state.technology]
    let kind2 = [...this.state.kind]
    let project_id_SendTemp = [...this.state.project_id_Send]

    if(e != 0 || data.length > 1) {

      data.splice(e, 1)
      kind2.splice(e,1)
      project_id_SendTemp.splice(e,1)

      this.setState({technology: data})
      this.setState({kind: kind2})
      this.setState({project_id_Send: project_id_SendTemp})

    }else {
      this.growl.show({severity: 'error', summary: 'Message', detail: 'Can\'t Delete'})
    }
  }
  //------------------ get kind_type_tech-----------//
  /*
   *   ใช้ 2 ที่คือ componentDidMount กับ technologyTypeChange
   *   idx คือตำแหน่ง ของ index (idx) e คือ id ของ project_type_tech
   *   ถ้าเปลี่ยน type  ก็ต้องมา query kind ใหม่  ส่วน Component ต้องใช้ตอน ที่ edit เพราะต้องไป load data มาใส่
   */
  getKind_type_tech(idx,e) {

    let self = this
    axios.get(url_server + '/kind_type_tech/' + e)
    .then(response => {
      if(Object.keys(response.data).length === 0){

        let data = [...this.state.kind]
        data[idx] = [{label:'', value:''}]
        self.setState({kind: data})
        // alert เตือน ถ้าไม่มีข้อมูล
        alert("ไม่มีข้อมูล กรุณาป้อน Kind ของ Type \"" + this.state.type[e-1].label + "\"")

      } else {
        //alert(JSON.stringify(response.data))
        let dataKind = [...self.state.kind]
        let free_data = [[{label:'', value:''}]]
        let free_data2 = [{label:'', value:''}]
        // check data first time
        if(Object.keys(dataKind).length === 0)
          dataKind = free_data

        if(idx != 0)
          dataKind[idx] = free_data2

        for (var i = 0; i < response.data.length; i++) {

          if(dataKind[idx].length <= i)
            dataKind[idx][i] = {label:'', value:''}

          dataKind[idx][i].value = response.data[i].Kind_type_tech_Id
          dataKind[idx][i].label = response.data[i].Name_kind_type_tech
        }
        self.setState({kind: dataKind})
      }
    })
    .catch(function (error) {
        alert('Error for query DB (catch react, getKind_type_tech)\n' + error)

    })
  }
  //----------------- end get kind_type_tech-----------//

  //-----------------------change State of select------------------------//
  technologyTypeChange(idx,e) {
    var stateCopy = Object.assign({}, this.state)
    stateCopy.technology[idx].kind = ''
    stateCopy.technology[idx].vendor = ''
    this.setState(stateCopy)

    this.getKind_type_tech(idx,e.value)

    var stateCopy = Object.assign({}, this.state)
    stateCopy.technology[idx].type = e.value
    this.setState(stateCopy)
  }

  technologyKindChange(idx,e) {
    var stateCopy = Object.assign({}, this.state)
    stateCopy.technology[idx].kind = e.value
    this.setState(stateCopy)
  }

  technologyVendorChange(idx,e) {
    var stateCopy = Object.assign({}, this.state)
    stateCopy.technology[idx].vendor = e.value
    this.setState(stateCopy)
  }
  //-------------------end-change State of select------------------------//

  render() {

    const dealStatusSelect = [{label:"ติดต่อ",value:"ติดต่อ"},
                              {label:"อีหยังบุ",value:"อีหยังบุ"},
                              {label:"ทำใบเสนอราคา",value:"ทำใบเสนอราคา"},
                              {label:"กำลังติดตั้ง",value:"กำลังติดตั้ง"},
                              {label:"เสร็จเรียบร้อย",value:"เสร็จเรียบร้อย"}
                             ]

    return(
            <div className="box" style={{marginTop:"10px",marginLeft:"6px",marginRight:"6px"}}>
              <p className="title is-1">{this.state._newProject? "New Project":"Edit Project"}</p>
              <div>
                <div className="tile is-ancestor" style={{width:"65%",marginLeft:"5%"}}>
                  <div className="tile is-parent is-vertical">

                    <div className="field is-horizontal">
                      <div className="field-label is-normal ">
                        Name Project
                      </div>
                      <div className="field-body">
                        <div className="field">
                          <div className="control">
                            <input
                              className="input is-info"
                              type="text"
                              placeholder="Name Project"
                              value={this.state.nameProject}
                              onChange={(e) => this.setState({nameProject: e.target.value})}
                            />
                          </div>
                        </div>
                      </div>
                    </div>

                    <div className="field is-horizontal">
                      <div className="field-label is-normal">
                        Amount
                      </div>
                      <div className="field-body">
                        <div className="field">
                          <div className="control" style={{width:'24%'}}>
                            <InputText
                              keyfilter="money"
                              className="input is-info"
                              type="text"
                              placeholder="มูลค่า"
                              value={this.state.amount}
                              onChange={ (e) => this.setState({amount: e.target.value})}
                            />
                          </div>
                        </div>
                      </div>
                    </div>

                    <div className="field is-horizontal">
                      <div className="field-label is-normal">
                        Estimate
                      </div>
                      <div className="field-body">
                        <div className="field">
                          <div className="control">
                            <Calendar
                              dateFormat="dd-mm-yy"
                              value={this.state.estimate}
                              onChange={(e) => this.setState({estimate: e.target.value})}
                              showIcon={true}
                            />
                          </div>
                        </div>
                      </div>
                    </div>

                      <div className="field is-horizontal">
                        <div className="field-label is-normal">
                          Deal Status
                        </div>
                        <div className="field-body">
                          <div className="field">
                            <div className="control">
                              <Dropdown
                                placeholder="Select a Deal Status"
                                value={this.state.dealStatus}
                                options={dealStatusSelect}
                                onChange={(e) => {this.setState({dealStatus:e.target.value})}}
                              />
                            </div>
                          </div>
                        </div>
                      </div>

                      {
                        this.state.technology.map((val, idx)=> {
                          let typeId = `type-${idx}`, kindId = `kind-${idx}`, vendorId = `vendor-${idx}`
                          return (
                            <div>
                              <div key={idx}>
                              <div style={{marginTop:"3px"}}>
                                <div className="field is-horizontal">
                                  <div className="field-label is-normal">
                                   <label htmlFor={typeId}>{`Type #${idx + 1}`}</label>
                                  </div>
                                  <div className="field-body">
                                    <div className="field">
                                      <div className="control">
                                        <Dropdown
                                          name={typeId}
                                          data-id={idx}
                                          id={typeId}
                                          placeholder="Select a Technology"
                                          options={this.state.type}
                                          value={this.state.technology[idx].type}
                                          onChange={this.technologyTypeChange.bind(this,idx)}
                                        />
                                      </div>
                                    </div>
                                    <div className="field-label is-normal">
                                      <label htmlFor={kindId}>Kind</label>
                                    </div>
                                    <div className="field">
                                      <div className="control">
                                            <Dropdown
                                              name={kindId}
                                              data-id={idx}
                                              id={kindId}
            																	placeholder="Select a Kind"
                                              options={this.state.kind[idx]}
            																	value={this.state.technology[idx].kind}
            																	onChange={this.technologyKindChange.bind(this,idx)}
            																/>
                                      </div>
                                    </div>
                                    <div className="field-label is-normal">
                                     <label>Vendor</label>
                                    </div>
                                    <div className="field-body">
                                      <div className="field">
                                        <div className="control">
                                          <Dropdown
                                            name={vendorId}
                                            data-id={idx}
                                            id={vendorId}
                                            placeholder="Select a Vendor"
                                            options={this.state.vendor}
                                            value={this.state.technology[idx].vendor}
                                            onChange={this.technologyVendorChange.bind(this,idx)}
                                          />
                                        </div>
                                      </div>
                                    </div>
                                    <div>
                                      <i className="far fa-trash-alt fa-2x" onClick={this.deleteSelect.bind(this,idx)}></i>
                                    </div>
                                  </div>
                                </div>
                                </div>
                              </div>
                            </div>
                           )
                         })
                      }
                      <div className="field is-horizontal">
                        <div className="field-label is-normal ">
                        </div>
                        <div className="field-body">
                          <div className="field">
                            <div className="control">
                              <i className="far fa-plus-square fa-3x" style={{margin:"1%"}} onClick={this.add.bind(this)}/>
                            </div>
                          </div>
                        </div>
                      </div>

                      <div className="field is-horizontal">
                        <div className="field-label is-normal ">
                        </div>
                        <div className="field-body">
                          <div className="field">

                            <a class="button is-info is-outlined" style={{marginRight:'1%'}}>
                              <span class="icon is-small">
                                <i class="fas fa-check"></i>
                              </span>
                              <span style={{width:'150px'}} onClick={this.saveProject.bind(this)}>
                                {this.state._newProject? "Save":"Update"}
                              </span>
                            </a>

                            <a class="button is-danger is-outlined" style={{marginLeft:'1%'}}>
                              <span style={{width:'150px'}} onClick={this.back.bind(this)}>Cancel</span>
                              <span class="icon is-small">
                                <i class="fas fa-times"></i>
                              </span>
                            </a>

                          </div>
                        </div>
                      </div>

                  </div>
                </div>
              </div>

              <Growl ref={(el) => this.growl = el} />

            </div>
          )
  }

}
