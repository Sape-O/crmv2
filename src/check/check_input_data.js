// function input , (comma) to number format
function formatNumber(num) {
	  return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
}
function onlyNumber(num) {
	// for Amount
	return /^\d+$/.test(num)
	//return //[+-]?([0-9]*[.])?[0-9]+.test(num)
}
// delete comma and push comma
function spritCommaNumber(num) {
	num = num.replace(/\,/g,'')
	num = Number(num)
	num = num.toFixed(2)
	return num
}

export {formatNumber, onlyNumber, spritCommaNumber}
